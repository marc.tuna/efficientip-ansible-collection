from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = '''
lookup: solidserver_ipam_ip_space_list
short_description: Look up the IP spaces in SOLIDserver IPAM.
description:
  - This module produces a list of all the available IPAM spaces contained 
    in a SOLIDserver
options:
  provider:
    description: 'variable list of parameters to access the SOLIDserver'
    version_added: '1.1.0'
    suboptions:
       host:
         description: 'name of the SOLIDserver to connect to'
         required: True
       authentication:
         description: 'how to authenticate'
         choices: ['basic', 'native']
         required: True
       username:
         description: 'name to use for the API call'
         required: True
       password:
         description: 'password associated with username'
         required: True
       check_tls:
         description: 'is the TLS certificate checked during connection'
         default: 'True'
       timeout:
         description: 'how long to wait for API to answer, in sec'
         default: '1'
  host:
    description: 'SOLIDserver hostname or IP address (if not using the provider)'
  authentication:
    description: 'Authentication mode (basic|native)  (if not using the provider)'
  username:
    description: 'Authentication username  (if not using the provider)'
  password:
    description: 'Authentication password  (if not using the provider)'
  check_tls:
    description: 'Check SSL certificate (default: True)  (if not using the provider)'
  order:
    description: 'Sort order'
  limit:
    description: 'Results limit'
  offset:
    description: 'Results offset'
'''

EXAMPLES = """
vars:
  spaces: "{{ lookup('solidserver_ipam_ip_space_list', host='192.168.1.1', authentication='basic', username='ipmadmin', password='password', wantlist=True) }}"

tasks:
- name: "use list return option and iterate as a loop"
  debug: msg="{% for space in spaces %}{{ space }} {% endfor %}"
# "Local AWS"

- name: "Pull IPAM spaces and print the default return style"
  debug: msg="{{ lookup('solidserver_ipam_ip_space_list', host='192.168.1.1', authentication='basic', username='ipmadmin', password='password') }}"
# "Local AWS"

 - name: import configuration of the SOLIDserver access
    include_vars:
      file: solidserver-creds.yml
      name: solidserver

 - name: list of spaces
    debug: msg="{{ lookup('community.efficientip.solidserver_ipam_ip_space_list', provider=provider, wantlist=True, ) }}"
    vars:
      provider: "{{ solidserver }}"
"""

RETURN = """
_raw:
  description: comma-separated list of IPAM spaces
"""


import logging
from ansible.errors import AnsibleError
from ansible.plugins.lookup import LookupBase
from ansible_collections.community.efficientip.plugins.module_utils.solidserver import solidserver


class LookupModule(LookupBase):
    def run(self, terms, variables, **kwargs):
        provider = {}
        order    = None
        limit    = None
        offset   = None
        
        if 'provider' in kwargs:
            provider = kwargs['provider']
        else:
            if 'host' not in kwargs:
                raise AnsibleError("Missing SOLIDserver host")
            if 'username' not in kwargs:
                raise AnsibleError("Missing SOLIDserver username")
            if 'password' not in kwargs:
                raise AnsibleError("Missing SOLIDserver password")
            provider = {
                'host': kwargs['host'],
                'username': kwargs['username'],
                'password': kwargs['password']
            }

        if 'check_tls' in kwargs:
            provider['check_tls'] = kwargs['check_tls']
            
        if 'authentication' in kwargs:
            if kwargs['authentication'] == 'native':
                provider['authentication'] = 'native'
            else:
                provider['authentication'] = 'basic'

        if 'order' in kwargs:
            order = str(kwargs['order'])
        if 'limit' in kwargs:
            limit = int(kwargs['limit'])
        if 'offset' in kwargs:
            offset = int(kwargs['offset'])

        sds = solidserver(provider)
        
        res, code, json = sds.ip_site_list(order, limit, offset)
        if res is False:
            raise AnsibleError("Failed to retrieve IPAM spaces")
        
        return [site['site_name'] for site in json]
